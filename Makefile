SRC = $(wildcard *.py goosync/*.py test/*.py)
TOX ?= py33,py27
TOXDIR = $(shell pwd)/.tox
OBJ = $(subst .py,.pyc,$(SRC))
ROOT ?=
PYCACHE = $(shell find . -type d -name __pycache__ -print | grep -v $(TOXDIR))
PIP ?= pip

ifneq ($(ROOT),)
	ROOT_ARGS = -r $(ROOT)
else
	ROOT_ARSG =
endif

TOX_TARGETS = -e $(TOX)
ifeq ($(TOX),)
	TOX_TARGETS =
endif

.PHONY: flake flake8
flake flake8:
	flake8 $(SRC)

.PHONY: package
package:
	python setup.py sdist bdist_wheel

# this only seems to work with python3
.PHONY: pypi distribute
pypi distribute: package
	python setup.py sdist bdist_wheel upload

.PHONY: tools
tools: TAGS glimpse

.PHONY: TAGS
TAGS: $(SRC)
	etags $^

.PHONY: glimpse
glimpse: $(SRC)
	glimpseindex -F -H `pwd` -E -B -n -o $<

.PHONY: test
test:
	(cd test && python test.py $(ROOT_ARGS) -n)

.PHONY: tox
tox:
	tox --recreate $(TOX_TARGETS)

.PHONY:
install:
	$(PIP) install --user --upgrade .

.PHONY:
uninstall:
	$(PIP) uninstall goosync

.PHONY: clean
clean:
	rm -rf $(OBJ) $(PYCACHE)

.PHONY: cleanall
cleanall: clean
	rm -rf $(TOXDIR) build goosync.egg-info
