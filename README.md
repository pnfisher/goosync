# Goosync

Goosync is a python script for synchronizing a remote google drive
with a local linux directory.

# Overview

This project was originally inspired by the github grive project.

* [https://github.com/Grive/grive](https://github.com/Grive/grive)

It works in much the same way.

I started this project when grive, which I had happily been using for
over a year, mysteriously stopped working. Instead of cloning and
debugging grive itself, I decided it would be more fun to write a new
version in python.

If grive is still working for you, then no need to bother with
goosync. But if, like me, you're having problems with grive, you
should find goosync to be a reliable, useful replacement for it.

# Installation

You can install it from PYPI as follows.

    sudo pip install goosync

Or.

    pip install --user goosync

Or you can clone this repository to your local linux system and from
within your cloned directory run the following command.

    sudo pip install .

Or.

    pip install --user .

Goosync works with python versions 2.6, 2.7, 3.3 and 3.4. I've tested
it thoroughly using all four of these versions.

# Usage

Goosync doesn't have a lot of bells and whistles. It synchronizes a
google drive repository to a single, specified root directory. If you
don't specify a specific root directory using the `-r` command-line
option, then goosync will default to using the current directory from
which it is being run.

The first time you run goosync, it will walk you through the standard
google drive authorization sequence (using google's own
google-api-python-client python package). Just do what it tells you.

After that, goosync will attempt to synchronize the authorized remote
google drive with the specified local directory (AKA local
repository). Basically, whatever resources (folders or files) that
exist on your google drive and don't exist in your local directory,
goosync will download. Whatever resources (directories and files)
that exist in your local directory but don't exist on your remote
google drive, goosync will upload. If there are resource conflicts,
where an item using the same name exists in the same folder/directory
in both locations, then goosync will attempt to figure out if the
items are identical using md5 checksums. If they are identical,
goosync will do nothing. If they aren't, goosync will prompt you for
conflict resolution guidance.

During this process, goosync will create and populate a `.goosync`
directory at the base of your local directory being synchronized.

During all subsequent runs (provided you don't delete or alter any of
the files in the local `.goosync` directory) goosync will carry out its
synchronization activities as follows.

* **Synchronization activities**

    If a file or folder is trashed or deleted remotely, and the
    corresponding local resource is unaltered since the last time goosync
    synchronized the drives, goosync will delete it locally. If the
    corresponding local resource is a directory, goosync will delete it,
    along with all of its contents.

    If a file or directory is created locally, and no corresponding
    remote resource has been created since the last time goosync
    synchronized the drives, goosync will upload that resource to the
    remote google drive. If the local resource is a directory, goosync
    will upload the directory itself, along with all of its contents.

    If a file or folder is created remotely, and no corresponding local
    resource has been created since the last time goosync synchronized
    the drives, goosync will download that resource to the local
    repository. If the remote resource is a folder, goosync will download
    the folder itself, along with all of its contents.

    If a file in your remote google drive has been modified since the
    last time goosync synchronized the drives, and the corresponding
    local file has not been modified, goosync will download that resource
    to the local repository.

    If a file in your local repository has been modified since the last
    time goosync synchronized the drives, and the corresponding remote
    file has not been modified, goosync will upload that resource to the
    remote repository. (Note, the upload will create a new google revision
    for the file -- it will not overwrite the original, thereby losing all
    of its existing revision history.)

    If a previously synchronized file or folder has been altered on your
    remote drive since the last time goosync synchronized the drives, and
    the corresponding resource is also altered in your local repository
    (or vice versa), goosync will prompt you for guidance on how to deal
    with the conflict. In general, goosync will consider resources to be
    in conflict when any of the following conditions hold.

    * **Conflicts**

        Two corresponding files have both been modified (including the
        possibility that one file has been modified while the other has been
        deleted, or vice versa) and the modifications have not left the md5
        checksums for the files in question with identical values.

        A remote or local folder or directory has been deleted, while some
        file contained in the corresponding folder or directory (or one of its
        descendant sub-folder or sub-directories) on the other drive has been
        modified or deleted.

# Things to note

Remote resources on your google drive are, when removal is called
for, trashed, not deleted. This means that in the case of accidents,
they are always salvageable through the google drive web interface.
Unfortunately, no such recovery option exists when things are deleted
locally. Given this, you may want to consider making a backup copy of
your local repository using rsync or some other convenient tool
before running goosync.

# Important command-line options

    --clone-local

Tells goosync to duplicate your local drive contents on your remote
google drive. Basically, everything on the local drive ends up on the
remote drive, anything on the remote drive not originally on the
local drive gets trashed. If you want to "clone" your remote google
drive locally, just create an empty directory and tell goosync to use
it as your local root repository (either using command-line argument
`-r` or by running from within the directory in question).

    -d, --dry-run

Tells goosync to perform a trial run without actually carrying out
any synchronization operations, and to report the result of its
trial-run calculations.

    -i, --ignore-conflicts

Tells goosync to ignore all resources suffering from synchronization
conflicts that would normally require your input in order to resolve
those conflicts. Any such ignored conflicts will be left in an
un-synchronized, unresolved state, requiring user guidance the next
time you rerun goosync (unless you again specify the -i command-line
option).

    --no-md5

Tells goosync not to use md5 checksums when files are in conflict in
order to determine if the modified files are still identical in
content.

    -Q, --query-renames

See **Limitations** below.

    -r LOCAL_DIRECTORY, --root-dir LOCAL_DIRECTORY

Informs goosync about the local directory with which it should
synchronize your remote google drive. If you don't specify a root
directory using -r, goosync will default to using the current
directory from which it is being run as the directory with which to
synchronize your remote google drive.

For information about additional command-line options, use the `-h,
--help` command-line option.

# Limitations

Currently, goosync does not handle google drive application files
(e.g. google docs). Goosync will not download converted copies of
these files to your local repository. If you store a google doc in a
google drive folder and at some point you delete the corresponding
directory in your local repository, goosync will not only not delete
the remote folder containing the google doc, but will recreate a
corresponding directory in your local repository. If you really want
to delete a remote google doc, you have to do it using the google
drive web interface.

Also, google drive allows you to create similarly named files within
the same folder. Unix does not. If your google drive contains such
similarly named files, goosync will ignore them. It will not attempt
to synchronize them to your local repository. It will treat them in
exactly the same way it treats your remote google docs. The same goes
for any remote file or folder using a name containing forward slashes
(i.e. `/`). If you want goosync to manage these types of remote
resources, you either need to rename them manually, or you need to
tell goosync to do it for you using its

    -Q, --query-renames

command-line option. This tells goosync to prompt you for renaming
options for all remote files and folders it finds using locally
unmanageable names.

# Caveats

You can't switch between using python versions 2 and 3 without first
deleting, or wiping `-w`, any pre-existing shelf database goosync uses
to track the resources it is managing. The shelve module (which
goosync uses) changed between python versions 2 and 3 such that a
shelf built using one version of python is not recognizable by the
other.

Whatever you do, don't look at test/test.py. It's pretty thorough, but
embarrassingly messy.

# Future considerations

Goosync may be seriously over-engineered -- mostly in order to solve a
few obscure corner cases to do with synchronizing resources that have
been deleted on one drive and at the same time modified on the other.
A lot of effort goes into managing a resource database tracking
modification times for every resource shared between both the google
drive and the local repository. I suspect all of this effort could be
avoided if goosync took a more relaxed attitude to these corner cases.
Currently, goosync prompts for user input when a resource goes missing
on one drive and has been modified on the other. If instead, goosync
assumed that modifications always trump deletions (i.e. if a resource
goes missing on one drive and has been modified on the other then go
ahead and synchronize the modified resource to the other drive instead
of asking for user guidance) all the effort needed to track
modification times for each individual resource would, I suspect, go
away. This would not only make the synchronization code easier to
understand and debug, but would also result in a less annoying
end-user experience (with goosync demanding less user feedback). As
long as its default behaviour in these corner cases was clearly
conveyed to the end-user, I suspect everyone would be happier. I may
decide to simplyify goosync in the future along these lines.

